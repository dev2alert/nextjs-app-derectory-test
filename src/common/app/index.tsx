import React from "react";
import {AppProvider} from "./Context";
import {AppGlobalStyled} from "./GlobalStyled";
import {AppLayout} from "./Layout";

export const App: React.FC<React.PropsWithChildren> = ({children}) => {
    return (
        <AppProvider>
            <AppLayout>{children}</AppLayout>
            <AppGlobalStyled />
        </AppProvider>
    );
};

export * from "./Context";
