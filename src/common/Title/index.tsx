"use client";

import styled from "styled-components";

export const Title = styled.h1`
    color: ${({theme}) => theme.palette.primary};
    font-family: sans-serif;
`;
